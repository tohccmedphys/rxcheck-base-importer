#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

try:
    readme = open('README.rst').read()
except:
    readme = ''
try:
    history = open('HISTORY.rst').read().replace('.. :changelog:', '')
except:
    history = ''

setup(
    name='rxcheck-base-importer',
    version='3.4.0',
    description='Base RxCheck package for implementing treatment planning system importers',
    long_description=readme + '\n\n' + history,
    author='Ryan Bottema',
    author_email='ryanbottema@gmail.com',
    url='https://bitbucket.org/tohccmedphys'
        '/rxcheck-base-importer',
    packages=[
        'rxcheck_base_importer',
    ],
    package_dir={'rxcheck_base_importer':
                 'rxcheck_base_importer'},
    include_package_data=True,
    install_requires=[
        "dvh",
    ],
    license="BSD",
    zip_safe=False,
    keywords='rxcheck-base-importer',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.10',
    ],
    test_suite='tests',
)
